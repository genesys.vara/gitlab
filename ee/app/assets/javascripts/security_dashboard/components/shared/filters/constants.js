import { s__ } from '~/locale';

export const CLUSTER_FILTER_ERROR = s__('SecurityOrchestration|Failed to load cluster agents.');
